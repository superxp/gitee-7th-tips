# 第三关
![书信](https://oschina.gitee.io/gitee-7th/img/level_3.53a48ed9.png)

```js
/** 转换为平仄，映射 0、1 */
const parseTone = (tone) => {
  return [3, 4].includes(tone)  ? 1 : 0;
};

const decode = (data) => {
  return data.map(bins => {
    return parseInt(bins.join(''), 2)
  });
};

const toChar = code => String.fromCharCode(code);

/** 按声调标注（轻声发音按单字原音） */
const tonesGroup = [
  [2, 4, 3, 2, 2, 1, 4, 1],
  [2, 4, 4, 4, 2, 4, 2, 4],
  [1, 4, 4, 1, 2, 4, 4, 4],
  [2, 4, 4, 4, 2, 1, 2, 2],
  [1, 4, 4, 2, 4, 4, 1, 2],
  [2, 4, 4, 1, 2, 1, 2, 3],
  [2, 3, 3, 2, 3, 4, 3, 2],
  [1, 4, 4, 1, 1, 4, 1, 4],
  [2, 4, 4, 4, 1, 4, 2, 1]
];

const binary = tonesGroup.map((tones) => {
  return tones.map(parseTone);
});

const answer = decode(binary).map(toChar).join('');

// console.log( 'binary:\n', binary );
console.log(answer);
```